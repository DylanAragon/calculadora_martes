import React from 'react';
import styled from 'styled-components';

const Teclado = styled.div`

background-color:black;
display: inline-block;

`

const Tecla = styled.button`

background-color: white;
color: black;
border: 1px soli black;
width:100px;
height:100px;
font-size:xxx-large;

`


export default (props) => {
    return(

        <Teclado>
            <Tecla onClick={() => props.pulsar(1)}>1</Tecla>
            <Tecla onClick={() => props.pulsar(2)}>2</Tecla>
            <Tecla onClick={() => props.pulsar(3)}>3</Tecla>
            <Tecla onClick={() => props.pulsar('*')}>*</Tecla>
            <br></br>
            <Tecla onClick={() => props.pulsar(4)}>4</Tecla>
            <Tecla onClick={() => props.pulsar(5)}>5</Tecla>
            <Tecla onClick={() => props.pulsar(6)}>6</Tecla>
            <Tecla onClick={() => props.pulsar('/')}>/</Tecla>
            <br></br>
            <Tecla onClick={() => props.pulsar(7)}>7</Tecla>
            <Tecla onClick={() => props.pulsar(8)}>8</Tecla>
            <Tecla onClick={() => props.pulsar(9)}>9</Tecla>
            <Tecla onClick={() => props.pulsar('-')}>-</Tecla>
            <br></br>
            <Tecla onClick={() => props.pulsar(0)}>0</Tecla>
            <Tecla onClick={() => props.pulsar('C')}>C</Tecla>
            <Tecla onClick={() => props.pulsar('=')}>=</Tecla>
            <Tecla onClick={() => props.pulsar('+')}>+</Tecla>
    
        
        </Teclado>
        
    )
}