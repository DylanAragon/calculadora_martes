import React, { useState } from 'react';
import styled from 'styled-components';
import Display from './display'
import Teclado from './teclado';


const Calculadora = styled.div`

background-color: #dedbdb;
border: 1px solid grey;
padding: 1px;
display: inline-block;
`


export default () => {

    const [valor1, setValor1] = useState('');
    const [valor2, setValor2] = useState('');
    const [operacion, setOperacion] = useState('');
    const [total, setTotal] = useState('');

    const pulsar = (x) => {

        if (x === '+') {
            console.log('estas sumando');
            setValor2(valor1);
            setValor1(0)
            setOperacion(x)
        }
        if (x === '-') {
            console.log('estas restando');
            setValor2(valor1);
            setValor1(0)
            setOperacion(x)
        }
        if (x === '*') {
            console.log('estas multiplicando');
            setValor2(valor1);
            setValor1(0)
            setOperacion(x)
        }
        if (x === '/') {
            console.log('estas dividiendo');
            setValor2(valor1);
            setValor1(0)
            setOperacion(x)
        } if (x === '=') {
            console.log('Calculando...')
            setValor1(valor1);
            if (operacion === '+') {
                const resultado = () => {
                    <>
                        {valor2 + valor1}
                    </>
                }
                setValor1(resultado)
            } if (operacion === '-') {
                const resultado = () => {
                    <>
                        {valor2 - valor1}
                    </>
                }
                setValor1(resultado)
            } if (operacion === '*') {
                const resultado = () => {
                    <>
                        {valor2 * valor1}
                    </>
                }
                setValor1(resultado)
            } if (operacion === '/') {
                const resultado = () => {
                    <>
                        {valor2 / valor1}
                    </>
                }
                setValor1(resultado)
            }
        }

        setValor1('' + valor1 + x)
    }


    return (
        <>

            <Calculadora>
                <Display valor={valor1} total={total} />
                <Teclado pulsar={pulsar} />
            </Calculadora>

        </>
    )
}